<?php
namespace Interso\Tests;

use Interso\NubexPrototypeAPI\Facade;
use PHPUnit\Framework\TestCase;

class PrototypeServiceTest extends TestCase
{
    protected function getUrl()
    {
        return 'http://localhost:8330/api';
    }

    public function testGetList()
    {
        $facade = new Facade($this->getUrl(), '222');

        $list = $facade->prototypes()->getList();
        $prot = $facade->prototypes()->get('1');
        $file = $facade->prototypes()->download('2', '/home/klementevilya/Documents/nubex-prototype-client/src/prototype');

        print_r($list);
        echo "{$list[0]->getMd5()}\n";
        print_r($prot);
        echo "{$prot->getMd5()}\n";
        echo $file;
    }
}