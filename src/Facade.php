<?php

namespace Interso\NubexPrototypeAPI;

use GuzzleHttp\Client as HttpClient;
use Interso\NubexPrototypeAPI\Service\PrototypeService;

class Facade
{
    /** @var string */
    protected $apiUrl;

    /** @var string */
    protected $apiKey;

    /** @var Client */
    protected $client;

    /** @var PrototypeService */
    protected $prototypeService;

    /**
     * Facade constructor.
     *
     * @param string $apiUrl
     * @param string $apiKey
     */
    public function __construct($apiUrl, $apiKey)
    {
        $httpClient = new HttpClient();
        $client = new Client($apiUrl, $apiKey, $httpClient);

        $this->prototypeService = new PrototypeService($client);
    }

    /**
     * @return PrototypeService
     */
    public function prototypes()
    {
        return $this->prototypeService;
    }
}