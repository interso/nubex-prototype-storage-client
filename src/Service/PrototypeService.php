<?php
namespace Interso\NubexPrototypeAPI\Service;

use GuzzleHttp\Exception\GuzzleException;
use Interso\NubexPrototypeAPI\Client;
use Interso\NubexPrototypeAPI\DTO\PrototypeDTO;
use Interso\NubexPrototypeAPI\Transformer\PrototypeTransformer;

class PrototypeService
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var PrototypeTransformer
     */
    private $transformer;

    /**
     * PrototypeCommand constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->transformer = new PrototypeTransformer();
    }

    /**
     * @param int $page
     * @param string $filter
     * @return array
     * @throws GuzzleException
     */
    public function getList($page = null, $filter = null)
    {
        $page
            ? $endpoint = sprintf('prototypes/%s', $page)
            : $endpoint = 'prototypes';

        $filter
            ? $endpoint = sprintf('%s?filter=%s', $endpoint, $filter)
            : null;

        $result = $this->client->get($endpoint);
        if (!$result) {
            return [];
        }

        return array_map(function ($datum) {
            return $this->transformer->transform($datum);
        }, $result);
    }

    /**
     * @param $id
     * @return PrototypeDTO|null
     * @throws GuzzleException
     */
    public function get($id)
    {
        $endpoint = sprintf('prototypes/%s', $id);

        $result = $this->client->get($endpoint);
        if (!$result) {
            return null;
        }

        $data = $result;
        return $this->transformer->transform($data);
    }

    public function download($id, $path)
    {
        $endpoint = sprintf('prototypes/%s/download', $id);

        $file = $this->client->download($endpoint, $path);
        return $file;
    }
}