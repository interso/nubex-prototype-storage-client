<?php

namespace Interso\NubexPrototypeAPI\Transformer;


use Interso\NubexPrototypeAPI\DTO\PrototypeDTO;

class PrototypeTransformer
{
    public function transform($data)
    {
        $prototype = new PrototypeDTO();

        $id     = isset($data['id'])    ? $data['id']       : null;
        $code   = isset($data['code'])  ? $data['code']     : null;
        $md5    = isset($data['md5'])   ? $data['md5']      : null;

        $prototype->setId($id);
        $prototype->setCode($code);
        $prototype->setMd5($md5);

        return $prototype;
    }
}